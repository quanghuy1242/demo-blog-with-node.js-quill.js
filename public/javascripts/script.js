var quill = new Quill('#editor-container', {
    modules: {
        syntax: true,
        toolbar: {
            container: '#toolbar-container',
            handlers: {
                'image': imageHandler
            }
        },
    },
    placeholder: 'Text here...',
    theme: 'snow'
});

function imageHandler() {
    var range = this.quill.getSelection();
    value = prompt('What is the image URL');
    this.quill.insertEmbed(range.index, 'image', value, Quill.sources.USER);
}

document.getElementById('btnShow').addEventListener('click', function() {
    var delta = quill.getContents();
    axios.post('/users', {
        data: delta
    })
    .then(function(res) {
        console.log(res);
    }).catch(function(e) {
        console.log(e);
    })
    
    axios.get('/users')
    .then(function(res) {
      var data = res.data;
      console.log(data);
    })
}, false);

