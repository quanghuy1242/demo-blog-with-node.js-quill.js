var express = require('express');
var router = express.Router();

var render = require('quill-render');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Demo for Quill.js' });
});

module.exports = router;
