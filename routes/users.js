var express = require('express');
var router = express.Router();

var QuillDeltaToHtmlConverter = require('quill-delta-to-html').QuillDeltaToHtmlConverter;

var axios = require('axios');

var data1;
var data2;

/* GET users listing. */
router.post('/', function(req, res, next) {
  data1 = req.body;
  console.log(data1);
});

router.get('/', function(req, res) {
  res.send(JSON.stringify(data1.data));
})

router.get('/view', function(req, res) {
  var deltaOps = data1.data.ops;
  var cfg = {};
  var converter = new QuillDeltaToHtmlConverter(deltaOps, cfg);
  var html = converter.convert();
  res.render('view', {
    html: html
  })
})

router.get('/upload', function(req, res) {
  console.log(data2);
  res.send(JSON.stringify(data2));
})

router.post('/upload', function(req, res) {
  data2 = req.body;
})

module.exports = router;
